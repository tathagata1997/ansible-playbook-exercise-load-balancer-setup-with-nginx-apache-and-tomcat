# Ansible Playbook Exercise Load Balancer Setup with Nginx Apache and Tomcat

## Overview

This repository contains the solution for Exercise 162, where the task is to write an Ansible playbook to set up Nginx as a load balancer, routing requests to Apache 2 and Tomcat nodes. The weights for routing are defined as follows: Apache 2 node (weight 5) and Tomcat node (weight 1).

## Prerequisites

- Ansible version: The playbook is designed to work with Ansible version that comes with Ubuntu 20.04.
- Nodes: The playbook assumes the presence of three nodes ('master.local', 'node1.local', 'node2.local', 'node3.local') with Ubuntu 20.04 installed.
- Ansible Inventory: Node details are declared in the 'inventory' file.
- Public/Private Keys: Ensure proper setup of public/private keys for secure communication.

## How to Run

- Clone this repository to your local machine:

```
git clone https://gitlab.com/your-username/your-repository.git

```
- Navigate to the 'feature162' branch:

```
git checkout feature162

```

- Run the Ansible playbook:

```
cd feature162
ansible-playbook -i inventory playbook.yml

```


## Task Description

The Ansible playbook, named 'playbook.yml', fulfills the following tasks:

- Installs Nginx on 'node1.local'.
- Installs Apache 2 on 'node2.local' with version '2.4.41-4ubuntu3.14' and sets up a valid HTML homepage containing the string 'dwsepqmnujzq'.
- Installs Tomcat on 'node3.local' with version '9.0.33' underneath '/usr/local' and sets up a valid HTML homepage containing the string 'xhpcmbcikznz'.

## Additional Information

- [Documentation: For more information about Ansible, visit](https://www.ansible.com/)
- [Nginx: Learn about Nginx's load balancer mode at](https://nginx.org/en/)
- [Apache2: Information about Apache2 is available at](https://httpd.apache.org/)
- [Tomcat: Information about Tomcat is available at](https://tomcat.apache.org/)
